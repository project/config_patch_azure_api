# Config Patch Azure API



# Setup
1. Visit /admin/config/development/config_patch.
   a. Set "Config base path." E.g., `config/default`.
   b. Set default output plugin to "Merge requestion in Azure with API"
2. Create a new Azure project access token
   a. In your Azure instance, visit https://dev.azure.com/[organization]/_usersSettings/tokens
   b. Name the Token "Config Patch".
   c. Grant code read & write scopes.
   d. Copy the token to your clipboard.
3. Visit /admin/config/config_patch_azure_api/credentials
   a. Enter your organization and project names.
   b. Paste in access token.
4. Visit /admin/config/config_patch_azure_api/repository_branch
   a. Select your repository.
   b. Select the target branch for new Config Patch merge requests.

# Usage
Try exporting config!

### UI Patch Export
Visit /admin/config/development/configuration/patch/config_patch_azure_api

### CLI Patch Export
`drush config:patch config_patch_azure_api --message="Exporting config from environment."`
