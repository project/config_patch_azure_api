<?php


namespace Drupal\config_patch_azure_api\Controller;


use Drupal\Component\Utility\Xss;
use Drupal\config_patch_azure_api\ConfigPatchAzureClient;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RepositoriesAutoCompleteController
 *
 * @package Drupal\config_patch_azure_api\Controller
 */
class RepositoriesAutoCompleteController extends ControllerBase {

  /**
   * @var \Drupal\config_patch_azure_api\ConfigPatchAzureClient
   */
  protected $client;

  /**
   * ProjectsAutoCompleteController constructor.
   *
   * @param \Drupal\config_patch_azure_api\ConfigPatchAzureClient $client
   */
  public function __construct(ConfigPatchAzureClient $client) {
    $this->client = $client;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Controller\ControllerBase|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_patch_azure_api.client')
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');
    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $projects = $this->client->getRepositories(['search' => $input]);
    foreach ($projects as $project) {
      $results[] = [
        'value' => $project['name'] . ' (' . $project['id'] . ')',
        'label' => $project['name'],
      ];
    }
    return new JsonResponse($results);
  }

}
