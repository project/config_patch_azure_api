<?php


namespace Drupal\config_patch_azure_api\Commands;


use Consolidation\AnnotatedCommand\AnnotationData;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class AzureApi
 *
 * @package Drupal\config_patch_azure_api\Commands
 */
class AzureApi extends DrushCommands {

  /**
   *
   * @hook option config:patch
   */
  public function additionalOption(Command $command, AnnotationData $annotationData) {
    if (!$command->getDefinition()->hasOption('message')) {
      $command->addOption(
        'message',
        '',
        InputOption::VALUE_REQUIRED,
        'Commit message'
      );
      $command->addOption(
        'start-branch',
        '',
        InputOption::VALUE_REQUIRED,
        'This is the branch from which the source branch will be created'
      );
      $command->addOption(
        'source-branch',
        '',
        InputOption::VALUE_REQUIRED,
        'This is the new branch that will be created with a new commit'
      );
      $command->addOption(
        'target-branch',
        '',
        InputOption::VALUE_REQUIRED,
        'The branch to which the merge request will be submitted'
      );
    }
  }

}
