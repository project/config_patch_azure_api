<?php

namespace Drupal\config_patch_azure_api\Form;

use Drupal\config_patch_azure_api\ConfigPatchAzureClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPatchAzureApiCredentialsForm.
 *
 * @package Drupal\config_patch_azure_api\Form
 */
class ConfigPatchAzureApiCredentialsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  const SETTINGS = 'config_patch_azure_api.credentials';

  /**
   * @var \Drupal\workflows\StateInterface
   */
  private $state;

  /**
   * @var \Drupal\config_patch_azure_api\ConfigPatchAzureClient
   */
  private $configPatchAzureClient;

  /**
   *
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    ConfigPatchAzureClient $config_patch_azure_client
  ) {
    parent::__construct($config_factory);
    $this->state = $state;
    $this->configPatchAzureClient = $config_patch_azure_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('config_patch_azure_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_patch_azure_api_credentials';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->state->get(static::SETTINGS);
    $form['organization'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Azure Organization'),
      '#default_value' => $settings['organization'] ?? '',
    ];

    $form['project'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Azure Project'),
      '#default_value' => $settings['project'] ?? '',
    ];

    $form['token'] = [
      '#type' => 'password',
      '#required' => TRUE,
      '#title' => $this->t('Azure Personal Access Token'),
      '#description' => $this->t('Create a personal access token with code read & write scope in Azure. Visit https://dev.azure.com/[organization]/_usersSettings/tokens.'),
      '#default_value' => $settings['token'] ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $token = $form_state->getValue('token');
    if (strlen($token) < 20) {
      $form_state->setErrorByName('token', $this->t('The token is too short.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set(static::SETTINGS, [
      'organization' => $form_state->getValue('organization'),
      'project' => $form_state->getValue('project'),
      'token' => $form_state->getValue('token'),
    ]);
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

}
