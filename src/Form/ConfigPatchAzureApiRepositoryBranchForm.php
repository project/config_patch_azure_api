<?php

namespace Drupal\config_patch_azure_api\Form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\config_patch_azure_api\Exception\MissingCredentialsException;
use Drupal\config_patch_azure_api\ConfigPatchAzureClient;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\State\StateInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPatchAzureApiRepositoryBranchForm
 *
 * @package Drupal\config_patch_azure_api\Form
 */
class ConfigPatchAzureApiRepositoryBranchForm extends FormBase {

  /**
   * Config patch settings state name.
   */
  const SETTINGS = 'config_patch_azure_api.repository_branch';

  /**
   * @var \Drupal\config_patch_azure_api\ConfigPatchAzureClient
   */
  protected $configPatchAzureClient;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * ConfigPatchAzureApiRepositoryBranchForm constructor.
   *
   * @param \Drupal\config_patch_azure_api\ConfigPatchAzureClient $config_patch_azure_client
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   */
  public function __construct(
    ConfigPatchAzureClient $config_patch_azure_client,
    UrlGeneratorInterface $url_generator,
    StateInterface $state,
    CacheBackendInterface $cacheBackend
  ) {
    $this->configPatchAzureClient = $config_patch_azure_client;
    $this->urlGenerator = $url_generator;
    $this->state = $state;
    $this->cache = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_patch_azure_api_repository_branch';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_patch_azure_api.client'),
      $container->get('url_generator'),
      $container->get('state'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    if (empty($form_state->getUserInput())) {
      $settings = $this->state->get(static::SETTINGS, []);
    } else {
      $settings = $form_state->getUserInput();
      $settings['repository_id'] = static::extractRepositoryIdFromAutocompleteInput($settings['repository_id']);
    }
    $current_project = NULL;
    if (!empty($settings['repository_id']) && $settings['repository_id']) {
      $current_project = $this->configPatchAzureClient->getRepositoryById($settings['repository_id']);
      if ($current_project) {
        $current_project = $current_project['name'] . ' (' . $settings['repository_id'] . ')';
      } else {
        $current_project = $settings['repository_id'];
      }
    }
    $wrapper_id = Html::getUniqueId('ajax-refresh');
    $form['settings'] = [
      '#type' => 'container',
      '#id' => $wrapper_id,
    ];
    $form['settings']['repository_id'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Repository'),
      '#autocomplete_route_name' => 'config_patch_azure_api.autocomplete.repositories',
      '#default_value' => $current_project,
      '#ajax' => [
        'event' => 'autocompleteclose',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading repository branches ...'),
        ],
        'callback' => [$this, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
        'method' => 'replace',
      ]
    ];

    $options = $this->getBranchOptions($form_state, $settings);

    $form['settings']['branch_name'] = [
      '#required' => TRUE,
      '#title' => $this->t('Target branch name'),
      '#description' => $this->t('This is the branch against which new merge requests will be created.'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => array_key_exists('branch_name', $settings) && in_array($settings['branch_name'], array_keys($options)) ? $settings['branch_name'] : '',
      '#states' => [
        'invisible' => [
          'select[name="project_id"]' => ['value' => ''],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];
    return $form;
  }

  /**
   * Returns the field value on ajax callback event change.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    $triggered_element = array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Extracts the project ID from the autocompletion result.
   *
   * @param string $input
   *   The input coming from the autocompletion result.
   *
   * @return mixed|null
   *   An entity ID or NULL if the input does not contain one.
   */
  public static function extractRepositoryIdFromAutocompleteInput($input) {
    $match = NULL;

    // Take "label (entity id)', match the ID from inside the parentheses.
    // @todo Add support for entities containing parentheses in their ID.
    // @see https://www.drupal.org/node/2520416
    if (preg_match("/.+\s\(([^\)]+)\)/", $input, $matches)) {
      $match = $matches[1];
    }

    return $match;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Extracts the entity ID from the autocompletion result.
    $repository_id = static::extractRepositoryIdFromAutocompleteInput($form_state->getValue('repository_id'));
    $this->state->set(static::SETTINGS, [
      'repository_id' => $repository_id,
      'branch_name'  => $form_state->getValue('branch_name'),
    ]);
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * @param \Drupal\config_patch_azure_api\Exception\MissingCredentialsException $e
   *
   * @return array
   */
  private function getMissingCredentialsResponse(MissingCredentialsException $e): array {
    $link = $this->urlGenerator->generateFromRoute('config_patch_azure_api.credentials');
    $message = $this->t($e->getMessage() . ' Please define it <a href="@link">here</a>.', [
      '@link' => $link
    ]);
    $this->messenger()->addWarning($message);
    return [];
  }

  /**
   * Gets the list of branches in the project.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $settings
   *
   * @return array
   */
  private function getBranchOptions(
    FormStateInterface $form_state,
    array $settings
  ) {
    $repository_id = empty($settings) ? $form_state->getValue('repository_id') : $settings['repository_id'];
    if (!$repository_id) {
      return ['' => $this->t('Invalid repository')];
    }
    $options = ['' => $this->t('-- Select branch --')];
    try {
      foreach($this->configPatchAzureClient->getBranches($repository_id) as $branch) {
        $options[$branch['name']] = ucfirst($branch['name']);
      }
      if (empty($options)) {
        $options[''] = $this->t('Repository has no branches');
      }
    } catch (RuntimeException $e) {
      $this->messenger()->addError($e->getMessage());
    }
    return $options;
  }

}
