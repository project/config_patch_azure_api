<?php declare(strict_types=1);

namespace Drupal\config_patch_azure_api\Exception;

class MissingCredentialsException extends \Exception {

}
