<?php


namespace Drupal\config_patch_azure_api\Plugin\config_patch\output;


use Drupal\config_patch\Plugin\config_patch\output\CliOutputPluginInterface;
use Drupal\config_patch\Plugin\config_patch\output\OutputPluginBase;
use Drupal\config_patch_azure_api\ConfigPatchAzureClient;
use Drupal\config_patch_azure_api\Exception\MissingCredentialsException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * Output patches as merge request in Azure.
 *
 * @ConfigPatchOutput(
 *  id = "config_patch_azure_api",
 *  label = @Translation("Merge requestion in Azure with API"),
 *  action = @Translation("Create Azure merge request")
 * )
 */
class AzureApi extends OutputPluginBase implements ContainerFactoryPluginInterface, CliOutputPluginInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorageExport;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\config_patch_azure_api\ConfigPatchAzureClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StorageInterface $config_storage_export, StateInterface $state, ConfigFactoryInterface $config_factory, ConfigPatchAzureClient $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configStorageExport = $config_storage_export;
    $this->config = $config_factory->get('config_patch.settings');
    $this->state = $state;
    $this->client = $client;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.storage.export'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('config_patch_azure_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function output(array $patches, FormStateInterface $form_state) {
    $list_to_export = [];
    $changes = $form_state->get('collections');

    if ($form_state->getValue('source_branch')) {
      $source_branch = $form_state->getValue('source_branch');
    }
    else {
      $source_branch = $this->getDefaultSourceBranchName();
    }

    foreach ($changes as $collection_name => $collection) {
      $list_key = $this->getListKey($collection_name);
      $list_to_export[$collection_name] = $form_state->getValue($list_key);
    }
    $actions = $this->collectAzureActions($changes, $source_branch, $list_to_export);
    $this->performAzureActions($actions, $form_state->getValue('commit_message'), $source_branch, $form_state->getValue('target_branch'), $form_state->getValue('start_branch'));
  }

  /**
   * @return string
   */
  public function getDefaultSourceBranchName(): string {
    return 'config-patch-' . time();
  }

  /**
   * {@inheritdoc}
   */
  public function outputCli(array $patches, array $config_changes = [], array $params = []) {
    if ($params['source-branch']) {
      $source_branch = $params['source-branch'];
    }
    else {
      $source_branch = $this->getDefaultSourceBranchName();
    }

    $actions = $this->collectAzureActions($config_changes, $source_branch);
    $this->performAzureActions($actions, $params['message'], $source_branch, $params['target-branch'], $params['start-branch']);
  }

  /**
   * Collects actions for commit api request.
   *
   * Read more here
   * https://learn.microsoft.com/en-us/rest/api/azure/devops/git/pushes/create?view=azure-devops-rest-5.1&tabs=HTTP
   * .
   *
   * @param array $changes
   *   Configuration changes grouped by collection name.
   * @param array $list_to_export
   *   List of configurations to export (limit).
   *
   * @return array
   *   Actions for the commit.
   * @throws \Http\Client\Exception
   */
  private function collectAzureActions(array $changes, $source_branch, array $list_to_export = []): array {
    $actions = [];
    $settings = $this->state->get('config_patch_azure_api.repository_branch');

    if ($this->client->getBranch($settings['repository_id'], $source_branch)) {
      $existing_branch = $source_branch;
    }
    else {
      $existing_branch = $settings['branch_name'];
    }

    $tree = $this->client->getTree($settings['repository_id'], $existing_branch, $this->config->get('config_base_path'));
    $existing_config_files = array_column($tree, 'relativePath');

    foreach ($changes as $collection_name => $collection) {
      $list = !empty($list_to_export[$collection_name]) ? $list_to_export[$collection_name] : array_keys($collection);

      foreach (array_filter($list) as $config_name) {
        $config_change_type = $changes[$collection_name][$config_name]['type'];
        if ($config_change_type === 'create') {
          $config_change_type = 'add';
        }
        if ($config_change_type === 'update') {
          $config_change_type = 'edit';
        }
        $file_path = $this->config->get('config_base_path') . '/' . $config_name . '.yml';
        if ($config_change_type === 'add' && array_search($file_path, $existing_config_files)) {
          $config_change_type = 'edit';
        }

        $source_name = $config_name;
        $target_name = $config_name;
        if ($collection_name != StorageInterface::DEFAULT_COLLECTION) {
          $target_storage = $this->configStorageExport->createCollection($collection_name);
        }
        else {
          $target_storage = $this->configStorageExport;
        }
        $raw_target = $target_storage->read($target_name);
        $target_data = $raw_target ? Yaml::encode($raw_target) : NULL;

        $base_dir = trim($this->config->get('config_base_path') ?? '', '/');
        if ($collection_name != StorageInterface::DEFAULT_COLLECTION) {
          $base_dir .= '/' . str_replace('.', '/', $collection_name);
        }
        $change = [
          'changeType' => $config_change_type,
          'item' => [
            'path' => ($base_dir ? $base_dir . '/' : '') . $target_name . '.yml',
          ]
        ];
        if ($config_change_type !== 'delete') {
          $change['newContent'] = [
            'content' => $target_data,
            'contentType' => 'rawtext'
          ];
        }
        $actions[] = $change;
      }
    }
    return $actions;
  }

  /**
   * Commit the changes and create merge request.
   *
   * @param array $actions
   * @param string $commit_message
   *   The commit message.
   * @param $source_branch
   * @param null $target_branch
   * @param null $start_branch
   */
  private function performAzureActions(array $actions, string $commit_message, $source_branch, $target_branch = NULL, $start_branch = NULL) {
    $settings = $this->state->get('config_patch_azure_api.repository_branch');
    if (!$target_branch) {
      $target_branch = $settings['branch_name'];
    }
    if (!$start_branch) {
      $start_branch = $target_branch;
    }
    try {
      if (empty($settings) || empty($settings['repository_id']) || empty($settings['branch_name'])) {
        throw new MissingMandatoryParametersException('Repository information is missing.');
      }
      $response = $this->client->createCommit($settings['repository_id'], $start_branch, $source_branch, $actions, $commit_message);
      if (!empty($response)) {
        if ($merge_request = $this->client->getOpenPullRequest($settings['repository_id'], $target_branch, $source_branch)) {
          $this->messenger()->addStatus($this->t('Pull request @merge_request_id was successfully updated, <a href="@url" target="_blank">view it here</a>.', [
            '@url' => $merge_request[0]['repository']['webUrl'] . '/pullrequest/' . $merge_request[0]['pullRequestId'],
            '@merge_request_id' => $merge_request[0]['pullRequestId'],
          ]));
        }
        else {
          $merge_request = $this->client->createPullRequest($settings['repository_id'], $target_branch, $source_branch, $commit_message);
          $this->messenger()->addStatus($this->t('New pull request @merge_request_id was successfully created, <a href="@url" target="_blank">view it here</a>.', [
            '@url' => $merge_request['repository']['webUrl'] . '/pullrequest/' . $merge_request['pullRequestId'],
            '@merge_request_id' => $merge_request['pullRequestId'],
          ]));
        }
      }
    }
    catch (MissingCredentialsException $e) {
      $link = Url::fromRoute('config_patch_azure_api.credentials');
      $message = $this->t($e->getMessage() . ' Please define it <a href="@link">here</a>.', [
        '@link' => $link->toString(),
      ]);
      $this->messenger()->addWarning($message);
    }
    catch (MissingMandatoryParametersException $e) {
      $link = Url::fromRoute('config_patch_azure_api.repository_branch');
      $message = $this->t($e->getMessage() . ' Please define it <a href="@link">here</a>.', [
        '@link' => $link->toString(),
      ]);
      $this->messenger()->addWarning($message);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function alterForm(array $form, FormStateInterface $form_state) {
    $form = parent::alterForm($form, $form_state);
    $settings = $this->state->get('config_patch_azure_api.repository_branch');
    try {
      if (empty($settings) || empty($settings['repository_id']) || empty($settings['branch_name'])) {
        throw new MissingMandatoryParametersException('Repository information is missing.');
      }
    }
    catch (MissingCredentialsException $e) {
      $link = Url::fromRoute('config_patch_azure_api.credentials');
      $message = $this->t($e->getMessage() . ' Please define it <a href="@link" target="_blank">here</a>.', [
        '@link' => $link->toString(),
      ]);
      $this->messenger()->addWarning($message);
      return [];
    }
    catch (MissingMandatoryParametersException $e) {
      $link = Url::fromRoute('config_patch_azure_api.repository_branch');
      $message = $this->t($e->getMessage() . ' Please define it <a href="@link" target="_blank">here</a>.', [
        '@link' => $link->toString(),
      ]);
      $this->messenger()->addWarning($message);
      return [];
    }
    $form['commit_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Commit message'),
      '#weight' => -100,
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'markup',
      '#title' => '<div><p>start branch => source branch</p></div>',
      '#weight' => -100,
      '#required' => TRUE,
    ];
    $form['start_branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start branch'),
      '#description' => $this->t('This is the branch from which the source branch will be created'),
      '#weight' => -100,
      '#required' => TRUE,
      '#default_value' => $settings['branch_name'],
    ];
    $form['source_branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source branch'),
      '#description' => $this->t('This is the new branch that will be created with a new commit'),
      '#weight' => -100,
      '#required' => TRUE,
      '#default_value' => 'config-patch-' . time(),
    ];
    $form['target_branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target branch'),
      '#description' => $this->t('The branch to which the merge request will be submitted'),
      '#weight' => -100,
      '#required' => TRUE,
      '#default_value' => $settings['branch_name'],
    ];

    return $form;
  }

  /**
   * Gets the list form element key according to collection name.
   *
   * This method is the copy of the same in config_patch.config_compare service.
   * Unfortunately it is not possible to inject the service here because of circular
   * reference.
   *
   * @param string $collection_name
   *   Collection name.
   *
   * @return string
   *   The key to be used in the form element.
   */
  private function getListKey($collection_name) {
    $list_key = 'list';
    if ($collection_name != StorageInterface::DEFAULT_COLLECTION) {
      $list_key .= '_' . preg_replace('/[^a-z0-9_]+/', '_', $collection_name);
    }
    return $list_key;
  }

}
