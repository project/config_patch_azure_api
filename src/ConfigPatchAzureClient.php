<?php

namespace Drupal\config_patch_azure_api;

use Drupal\config_patch_azure_api\Form\ConfigPatchAzureApiCredentialsForm;
use Drupal\config_patch_azure_api\Form\ConfigPatchAzureApiRepositoryBranchForm;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;

/**
 * Class ConfigPatchAzureClient.
 *
 * @package Drupal\config_patch_azure_api
 */
class ConfigPatchAzureClient {

  /**
   * Azure API core server.
   */
  const API_HOST = 'https://dev.azure.com/';

  /**
   * Azure API version.
   */
  const API_VERSION = '5.0';

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $credentials;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $repository;

  /**
   * ConfigPatchAzureClient constructor.
   *
   * @param \GuzzleHttp\Client $client
   */
  public function __construct(Client $client, StateInterface $state) {
    $this->client = $client;
    $this->credentials = $state->get(ConfigPatchAzureApiCredentialsForm::SETTINGS, []);
    $this->repository = $state->get(ConfigPatchAzureApiRepositoryBranchForm::SETTINGS, []);
  }

  /**
   * Gets the repositories that meet the required conditions from parameters.
   *
   * @param array $parameters
   *   Array of parameters for repository search.
   *   See more here: https://learn.microsoft.com/en-us/rest/api/azure/devops/git/repositories/list?view=azure-devops-rest-6.0&tabs=HTTP
   *
   * @return array
   *   The array of repositories with properties as array.
   */
  public function getRepositories(array $parameters = []): array {
    $repositories = $this->request('repositories');
    if (!empty($repositories) && !empty($parameters['search'])) {
      foreach ($repositories as $key => $repository) {
        if (stripos($repository['name'], $parameters['search']) === FALSE) {
          unset($repositories[$key]);
        }
      }
    }
    return $repositories;
  }

  /**
   * Gets the project by Id.
   *
   * @param $id
   *   Id of the project.
   *
   * @return array
   */
  public function getRepositoryById($id) {
    return $this->request('repositories/' . $id);
  }

  /**
   * Get branches per project.
   *
   * @param $repository_id
   *   Id of the project.
   *
   * @return array
   * @throws \Http\Client\Exception
   */
  public function getBranches($repository_id, $filter = TRUE): array {

    $branches = $this->request('repositories/' . $repository_id . '/refs');

    if (!empty($branches)) {
      foreach ($branches as $key => $branch) {
        // Ignore tag branches.
        if ($filter) {
          if (strpos($branch['name'], 'refs/heads/') === FALSE || strpos($branch['name'], 'refs/heads/config-patch-') !== FALSE) {
            unset($branches[$key]);
          } else {
            $branches[$key]['name'] = str_replace('refs/heads/', '', $branch['name']);
          }
        }
      }
    }

    return $branches;
  }

  /**
   * @param $repository_id
   * @param $branch_name
   *
   * @return array|NULL
   */
  public function getBranch($repository_id, $branch_name, $filter = TRUE) {
    $branches = $this->getBranches($repository_id, $filter);
    foreach ($branches as $branch) {
      if ($branch['name'] === $branch_name) {
        return $branch;
      }
    }
    return NULL;
  }


  /**
   * Get the list of commits from the branch.
   *
   * @param $repository_id
   *   Id of the project.
   * @param string $branch_name
   *   Branch name.
   *
   * @return array
   */
  public function getCommits($repository_id, string $branch_name): array {
    return $this->request('repositories/' . $repository_id . '/commits?searchCriteria.itemVersion.version=' . $branch_name);
  }

  /**
   * Get the difference between the branch and latest commit.
   *
   * @param $repository_id
   *   Id of the project.
   * @param string $branch_name
   *   Branch name.
   *
   * @return string
   */
  public function diff($repository_id, string $branch_name) {
    return $this->request('repositories/' . $repository_id . '/diffs/commits');
  }

  /**
   * Create the commit in Azure repository.
   *
   * @param $repository_id
   *   Id of the project.
   * @param string $start_branch
   *   The start branch from which the new one with the commit will be created.
   * @param string $branch_name
   *   The name of the new branch.
   * @param array $actions
   *   The list of files that were changed. Read more here https://learn.microsoft.com/en-us/rest/api/azure/devops/git/pushes/create?view=azure-devops-rest-5.1&tabs=HTTP
   * @param string $message
   *   Commit message (required by Azure).
   *
   * @return mixed
   */
  public function createCommit($repository_id, string $start_branch, string $branch_name, array $actions, string $message = '') {
    $existing_branch = $this->getBranch($repository_id, 'refs/heads/' . $branch_name, FALSE);
    if (!empty($existing_branch['objectId'])) {
      $objectId = $existing_branch['objectId'];
    } else {
      $branch_from = $this->getBranch($repository_id, $start_branch);
      $objectId = $branch_from['objectId'];
    }

    $params = [
      'refUpdates' => [
        [
          'name' => 'refs/heads/' . $branch_name,
          'oldObjectId' => $objectId
        ]
      ],
      'commits' => [
        [
          'comment' => $message,
          'changes' => $actions
        ]
      ]
    ];
    return $this->request('repositories/' . $repository_id . '/pushes', $params, 'POST');
  }

  /**
   * Create pull request in Azure.
   *
   * @param $repository_id
   *   Id of the project.
   * @param string $target_branch
   *   The branch that will be merged.
   * @param string $branch_name
   *   The branch where the changes from $start_branch will be merged.
   * @param string $title
   *   The title of Merge Request.
   *
   * @return mixed
   */
  public function createPullRequest($repository_id, string $target_branch, string $branch_name, string $title) {
    return $this->request('repositories/' . $repository_id . '/pullrequests', [
      'sourceRefName' => 'refs/heads/' . $branch_name,
      'targetRefName' => 'refs/heads/' . $target_branch,
      'title' => $title
    ], 'POST');
  }

  /**
   * @param $repository_id
   * @param $target_branch
   * @param $branch_name
   *
   * @return mixed
   */
  public function getOpenPullRequest($repository_id, $target_branch, $branch_name) {
    return $this->request('repositories/' . $repository_id . '/pullrequests?searchCriteria.sourceRefName=' . $branch_name . '&searchCriteria.targetRefName=' . $target_branch);
  }

  /**
   * @param $repository_id
   * @param string $branch_name
   *
   * @return mixed
   * @throws \Http\Client\Exception
   */
  public function getTree($repository_id, string $branch_name, $path) {
    $tree = FALSE;
    $items = $this->request('repositories/' . $repository_id . '/items?scopePath=' . $path . '&versionDescriptor.versionType=branch&versionDescriptor.version=' . $branch_name);
    if (!empty($items)) {
      $tree = $this->request('repositories/' . $repository_id . '/trees/' . $items[0]['objectId'] . '?recursive=true');
      if (!empty($tree)) {
        return $tree['treeEntries'];
      }
    }
    return $tree;
  }

  /**
   * Build request API URL.
   *
   * @param $uri
   * @return string
   */
  protected function getAPIUrl($uri) {
    $url = self::API_HOST . $this->credentials['organization'] . '/' . $this->credentials['project'] . '/_apis/git/' . $uri;
    if (strpos($uri, "?") !== FALSE) {
      $url .= '&api-version=' . self::API_VERSION;
    } else {
      $url .= '?api-version=' . self::API_VERSION;
    }
    return $url;
  }

  /**
   * Send request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   API response.
   */
  protected function request($uri, $data = [], $method = 'GET') {
    $args = [
      'headers' => [
        'Authorization' => ['Basic ' . base64_encode('Basic:' . $this->credentials['token'])],
      ],
    ];

    switch ($method) {
      case 'POST':
        $args['headers']['Content-Type'] = 'application/json';
        $args['body'] = \GuzzleHttp\json_encode($data);
        $response = $this->client->post($this->getAPIUrl($uri), $args);
        break;
      default:
        $response = $this->client->get($this->getAPIUrl($uri), $args);
        break;
    }
    $content = $response->getBody()->getContents();
    $content = json_decode($content, TRUE);
    if (!empty($content)) {
      if (isset($content['value'])) {
        return $content['value'];
      }
      return $content;
    }
    return FALSE;
  }

}
